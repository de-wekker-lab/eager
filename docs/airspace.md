# Airspace Information

## Introduction
This page contains airspace authorization information for the NSF EAGER Campaign.

## Airspace Authorization Application

### Operational Title: 
KTPH - UAS Research Flights 0.5 NM SSE of RW 33

### Proposed Location of Operation:

Flight operations will take place within a 0.75 NM radius circle centered at 38° 2' 27.6" N, 117° 4' 48.6012" W, approximately 0.5 NM south southeast of runway 33 at Tonopah Airport (KTPH) in Tonopah, Nevada. Eight quadrotors will be flown simultaneously at altitudes of 4 m, 7 m, 17 m, and 27 m within 5 m of either one 106ft tower (approximate location: 38° 2' 17.4588" N, 117° 5' 14.82" W) or a 15ft tower array (approximately from: 38° 2' 35.5812" N, 117° 4' 32.34" W to: 38° 2' 37.7412" N, 117° 4' 22.3788" W). Meteorological tower installation coordinates are approximate. As such, this request is for a 0.75 NM radius covering the approximate locations of the meteorological towers. The flights will take place within 0 ft grids in class E2 airspace, but at no point will sUAS overfly runways, taxiways, ramps, or airport facilities. The location, site plan, and experimental design is available at this URL for further clarification: https://arcg.is/14rz8P0

### Proposed Maximum Altitude (AGL): 
100 ft 

### Latitude
38° 2' 27.6" N

### Longitude
117° 4' 48.6012" W

### Description of Your Proposed Operations
Eight university-trained Part 107 remote pilots will fly pre-planned autonomous missions located within 5 m of fixed meteorological towers (location description in Proposed Location of Operation section). All flights will strictly be a straight-and-level hover within VLOS for 20 minutes per hour at a designated altitude (4 m, 7 m, 17 m, and 27 m). Operations will take place from sunrise to sunset daily beginning 13 August 2023 and ending 22 August 2023. Two Part 107 remote pilots will act as ground crew overseeing the operation - one will act as the visual observer (VO) and one as the "Air Boss" (AB). The following procedures and restrictions will be in place for all flights:

1. All operations will comply with 14 CFR Part 107.
2. Each PIC will perform a written hazard assessment on the first day of the flight campaign before launch.
3. All PICs will participate in a daily safety and mission briefing led by the AB to include a review of current NOTAMs and TFRs, operational overview, and hazard mitigations.
4. Each PIC will be assigned a single aircraft to maintain for the duration of the campaign to ensure consistent flight parameters.
5. Return-to-home parameters will be set at even 10 ft increments (10 ft - 80 ft) and assigned to each PIC for the duration of the campaign to avoid mid-air collisions.
6. All sUAS will be software limited to 100 ft AGL.
7. All sUAS will have lost link contingency set to return-to-home immediately. 
8. The AB will carry two airband radios tuned to CTAF frequency @ 123.00MHz and emergency frequency @ 121.50 MHz.
9. The AB will issue a UOA with NOTAM via  www.1800wxbrief.com  for the duration of the flight period at least 24 hours prior to operations.

Operational visuals attached and available online: https://arcg.is/14rz8P0

### Application Reference Number:
Expired: 2023-P107-WSA-18638
Current: 2023-P107-WSA-23207

## Airspace Storymap
Additional airspace information can be found in the [Airspace Storymap](https://storymaps.arcgis.com/stories/e9ce0cc006a24f4e819daffa2d45dfc1) embedded below.
<iframe src="https://storymaps.arcgis.com/stories/e9ce0cc006a24f4e819daffa2d45dfc1" width="100%" height="500px" frameborder="0" allowfullscreen allow="geolocation"></iframe>

## Certificate of Authorization Letter
[Link to Authorization Letter](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/airspace/FAA%20Form%207711-1%202023-P107-WSA-23207_KTPH.pdf#toolbar=0)
<iframe src="/eager/resources/airspace/FAA%20Form%207711-1%202023-P107-WSA-23207_KTPH.pdf" width="100%" height="900"></iframe>

## COA Modification
[Email Response from 57 Operations Support Squadron](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/airspace/RE:%20UAS%20Operations%20-%20KTPH.pdf#toolbar=0)
<iframe src="/eager/resources/airspace/RE%20UAS%20Operations%20-%20KTPH.pdf" width="100%" height="900"></iframe>


