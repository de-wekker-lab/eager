# Operation Guide

## Introduction

The steps below describe the research operational procedures and assignments that must be followed throughout the research project.


## Assignments

PICs and VOs are assigned in the [Attendee Information/Assignments](https://purdue0-my.sharepoint.com/:x:/g/personal/rose196_purdue_edu/EbDaUUNqKshKnUhxuibfdGABuqDOJR7SwCqoXc4KpgUeZg?e=FQOjfz) tab.

## Overview

### Pilot Responsibilities
- [ ] Complete all [checklists](checklists.md)
- [ ] Set DJI [app settings](app_settings.md)
- [ ] Set [contingency settings](contingency_settings.md)
- [ ] Verify [Emergency Procedures](#emergency-procedures)

### VO Responsibilities
- [ ] Complete Pre-Flight Checklist with PIC.
- [ ] Record [Metadata](data_collection.md#metadata)
- [ ] Validate flight plan with the PIC.
- [ ] Coordinate launch time with Operational Manager
- [ ] Maintain situational awareness of airspace

### Experimental Design
Each PIC will be paired with a single VO and a single aircraft for the duration of the project and assigned a flight location for each day of the project. There will be five days of flights in the configuration shown in the figures below. Review the photos below to acclimate to the flight area. The experimental design will be finalized before flights begin.

<center>
<figure>
  <img src="../img/operation/ktph_experimentalDesign.png" width="100%" alt="experimental design">
  <figcaption><small><small><br>Experimental design showing flight plan for five days of flights.</small></small></figcaption>
</figure>
</center>

<center>
<figure>
  <img src="../img/operation/ktph_towerArray1.jpg" width="500" alt="4m sonic array">
  <figcaption><small><small><br>Picture 1 of the 4m tower array.</small></small></figcaption>
</figure>
</center>

<center>
<figure>
  <img src="../img/operation/ktph_towerArray2.jpg" width="500" alt="4m sonic array">
  <figcaption><small><small><br>Picture 2 of the 4m tower array.</small></small></figcaption>
</figure>
</center>

<center>
<figure>
  <img src="../img/operation/ktph_towerArray3.jpg" width="500" alt="4m sonic array">
  <figcaption><small><small><br>Picture 3 of the 4m tower array.</small></small></figcaption>
</figure>
</center>

<center>
<figure>
  <img src="../img/operation/ktph_towerArray4.jpg" width="500" alt="4m sonic array">
  <figcaption><small><small><br>Picture 4 of the 4m tower array.</small></small></figcaption>
</figure>
</center>

<center>
<figure>
  <img src="../img/operation/ktph_mainTower1.jpg" width="500" alt="4m sonic array">
  <figcaption><small><small><br>Picture 27m tower.</small></small></figcaption>
</figure>
</center>




## Safety

### Safety Overview
UAS flights for this operation will take place in class E2 airspace between sunrise and sunset at the locations specified in [Airspace Information](airspace.md). Nathan will serve as the air boss (AB) for all operations and will act to guide pilot operation and monitor/de-conflict airspace. Pilots in command (PICs) will have the ultimate and final say in flight operations of their assigned aircraft. 

The approved area is defined in the Operations Area Map below
<center>
<figure>
  <img src="../img/op_area_map.jpg" width="100%" alt="Operations Area Map">
  <figcaption><small><small><br>Operations Area Map as approved in FAA Airspace Authorization document</small></small></figcaption>
</figure>
</center>

### Visual Observer
Per section 2.f. of the FAA Airspace Authorization document, each PIC is required to be partnered with a VO for all flights. VOs will be assigned to a PIC for the duration of the campaign. PICs and VOs will develop  to work together to practice 

### Emergency Procedures
The following procedures should be implemented should the conditions arise.

#### Operational Stop
In the event that the operations need to be stopped, **ANY** PIC or VO has authority to signal cancellation of the operation. The signal to stop operations immediately is for the individual(s) to audibly yell "STOP" while waving their hands above their heads until the aircraft have landed and the threat has been identified by all operators.

#### Lost link
In the event of a lost link, the flight controller has been programmed to return to home. As soon as a lost link event is recognized by the PIC, he or she is to alert the AB and declare emergency intentions to other PICs. All other aircraft are to abort the mission and give way to the aircraft in emergency status by maintaining altitude and moving at least 10m horizontally to clear the expected landing path of the emergency status aircraft. 

#### Lost GPS
Because the UAS used in this data campaign rely on GPS for most emergency procedures (hover, RTH), a lost of GPS will not trigger any pre-programmed procedure. The PIC can expect an audio/visual cue from the RC controller/transmitter announcing that the aircraft is in attitude mode (ATTI). Should a loss of GPS occur, PICs should alert the AB and declare emergency intentions to surrounding PICs. The PIC should remain aware of the aircraft orientation at all times and land the aircraft manually as quickly and safely as possible ideally following a straight, vertical path to the launch location. All other aircraft are to abort the mission and give way to the aircraft in emergency status by maintaining altitude and moving at least 10m horizontally to clear the expected landing path of the emergency status aircraft. 

#### Uncooperative aircraft.
Manned aircraft have right-of-way over UAS. If a manned aircraft enters the operational airspace, the observing PIC/VO/AB/etc. will announce clock position  of the aircraft. All PICs should abort the mission and land immediately.

#### Low/Critical Battery
All UAS will be programmed to alert the PIC at 20% battery life and RTH at 10% battery. This safety margin will allow the PIC to accommodate environmental and/or operational delays. 

#### LiPo Battery Fire
In the event of an **in-flight LiPo battery fire**, the PIC should declare an emergency to the AB and surrounding PICs and land as quickly and safely as possible. The PIC should also avoid people, equipment, and other property while aiming for a wide open space. The PIC and all operators should assume the UAS can not be recovered and avoid approaching the UAS until the smoke dies off. However, if a fire extinguisher is available, any crew member is permitted, but not required, to attempt to extinguish the fire with suppressant. 

In the event of a **fire while charging** and if the crew member is able/willing, the battery should be relocated outside asap to avoid smoke inhalation and structure fire. The crew should either attempt to extinguish the fire with the appropriate fire extinguisher or allow it to burn out. Regardless of approach, the crew should evacuate the area and only stand up wind of the LiPo fire. 

#### Flyaway
In the rare case of a UAS flyaway, the PIC must alert the AB who will work with the VO to gather information while the PIC attempts to regain control. The AB will then alert the FAA/ATC as required. The PIC should continually try to regain control of the aircraft while other crew members assist in visually observing the airspace.