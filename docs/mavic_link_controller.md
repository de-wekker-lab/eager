# Mavic Platinum Pro: How to Pair Backup Controller as Primary

1. Power on the aircraft, controller, and smart phone

<center>
<figure>
  <img src="../img/mavic_link_controller/MPP_1.PNG" width="500">
</figure>
</center>

2. Tap the three dots in the upper right hand corner. You should see that the only menu option is for the remote controller

<center>
<figure>
  <img src="../img/mavic_link_controller/MPP_3.PNG" width="500">
</figure>
</center>

3. Tap the remote controller option and scroll to the bottom 

<center>
<figure>
<img src="../img/mavic_link_controller/MPP_4.PNG" width="500">
</figure>
</center>

5. Press the *"Linking Remote Controller"* button

<center>
<figure>
  <img src="../img/mavic_link_controller/MPP_5.PNG" width="500">
</figure>
</center>

6. When prompted with the menu shown above, press *"Primary Remote Controller"* 

<center>
<figure>
  <img src="../img/mavic_link_controller/MPP_6.PNG" width="500">
</figure>
</center>

7. If your pairing was successful you should see all the menu options on the side bar which can be seen above

*** 

Document written by [Aaron Reilly](mailto:reilly28@purdue.edu)