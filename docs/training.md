# Training Resources
This page contains links to all of the training resources.

## [Training Presentation](https://purdue0-my.sharepoint.com/:p:/g/personal/rose196_purdue_edu/Ef-LacdbeDtMmBTARy-R9NoB6dIyfRJtjZTVcxeIQioLZA?e=MEoB5D)
<iframe src="https://purdue0-my.sharepoint.com/personal/rose196_purdue_edu/_layouts/15/Doc.aspx?sourcedoc={c7698bff-785b-4c3b-9814-c0472f91f4da}&amp;action=embedview&amp;wdAr=1.7777777777777777" width="476px" height="288px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

## [Airport Training](training_airspace.md)

## [Desert Training](training_desert.md)