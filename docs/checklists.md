# Checklists

## Pre-operation

??? note "Pre-operation Checklist"
    - [ ] Experimental Briefing
    - [ ] Operational Briefing
    - [ ] Briefing between VO & PIC
    - [ ] Install / Verify landing pad (orientation)
    - [ ] Weather debriefing [AWC METAR](https://www.aviationweather.gov/metar/data?ids=KTPH&format=decoded&date=&hours=0&taf=on) or [GFA](https://www.aviationweather.gov/gfa) if METAR inoperative.
    - [ ] Airspace & Flight Restrictions [UAS Facilities Map](https://faa.maps.arcgis.com/apps/webappviewer/index.html?id=9c2e4406710048e19806ebf6a06754ad&extent=-13091409.8659%2C4551456.8832%2C-12975837.0791%2C4620173.7716%2C102100)
    - [ ] Verify sonic anemometer is live [Sonic Data](http://datavis.eol.ucar.edu/ncharts/projects/M2HATS/noqc_geo)
    - [ ] Confirm contingency plans and emergency procedures. See [Contingency Settings](contingency_settings.md) page.
    - [ ] Verify that DJI [App Settings] have been applied correctly.

## Pre-flight

??? note "Pre-flight Checklist"
    - [ ] Inspect aircraft
    - [ ] Check battery percentage
    - [ ] Clear area
    - [ ] Confirm flight plans with VO
    - [ ] Remove gimbal cover
    - [ ] Verify control link
    - [ ] Verify GPS

## Post-flight

??? note "Post-flight Checklist"
    - [ ] Record 
    - [ ] Inspect aircraft
    - [ ] Replace battery
    - [ ] Verify RC controller charge (If RC has been on for 2 hours, bind new RC & charge old RC)

## Post-operation  

??? note "Post-operation Checklist"
    - [ ] Download Data
    - [ ] Secure aircraft from sun & sand
    - [ ] Carry/ Pack drone & charges

## End-of-Day
This will take place at dinner and/or at hotel

??? note "End-of-Day Checklist"
    - [ ] Debrief with all hands
    - [ ] All batteries on charge (or have plan to complete charge)
    - [ ] Complete [Flight Log](https://purdue0-my.sharepoint.com/:x:/g/personal/rose196_purdue_edu/ETrX6SV0bMZJglDvitd1KOwB89-BTYLAgjGTkB19_AnVWA?e=F5T0ng) spreadsheet.
    - [ ] Upload photos to the [box link](https://purdue.app.box.com/f/91a73535fa114e87b768488dd054521a)

## Operational Manager

??? note "Operational Manager Checklist"
    - [ ] Call ATC @ 702-652-9612/3275/3278 (Call 30 mins before operation)
        - Primary telephone number of the operator: 765-413-6306
        - Schedule of flight (time and duration)
        - Location where flight will occur (Coordinates - 38°02'19.2"N 117°04'24.2"W & Radius - 0.75 NM)
        - Altitude
        - Verify the currect weather (Ceiling - 1000 ft. AGL and Visibility) [KTPH METAR](https://www.aviationweather.gov/metar/data?ids=ktph&format=raw)
    - [ ] Monitor CTAF @ 123.0 MHz 





