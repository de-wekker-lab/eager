# Aircraft Information

## [Inventory](inventory.md)

## Important Aircraft Notes

### DJI Mavic Pro Platinum B
- Gimbal motor inoperative. The camera image jumps around and shows "Gimbal Obstructed" error message. Aircraft appears to fly nominally.

## Registrations

**Mavic Pro Platinum: [A](#mppA) | [B](#mppB) | [C](#mppC) | [D](#mppD) | [S](#mppS)**

**Mavic Mini: [E](#mmE) | [S](#mmS)**

**Mavic Mini SE: [A](#mmA) | [B](#mmB) | [C](#mmC) | [D](#mmD) | [S](#mmS)**

**Mavic Mini 3: [A](#mm3A)**

**Holybro Drone [S](#hbdS)**

### <a name="mppA"></a>Mavic Pro Platinum A

<iframe src="/eager/resources/uasreg/uva_mpp_a_FA3KY4AK9F.pdf" width="100%" height="250px"></iframe>


### <a name="mppB"></a>Mavic Pro Platinum B

<iframe src="/eager/resources/uasreg/uva_mpp_b_FA3KY4EEC7.pdf" width="100%" height="250px"></iframe>


### <a name="mppC"></a>Mavic Pro Platinum C

<iframe src="/eager/resources/uasreg/uva_mpp_c_FA3KY4H9FW.pdf" width="100%" height="250px"></iframe>


### <a name="mppD"></a>Mavic Pro Platinum D

<iframe src="/eager/resources/uasreg/uva_mpp_d_FA3PH4P74W.pdf" width="100%" height="250px"></iframe>


### <a name="mppS"></a>Mavic Pro Platinum S

<iframe src="/eager/resources/uasreg/sfdw_mpp_FA3RHX7ELY.pdf" width="100%" height="250px"></iframe>


### <a name="mmE"></a>Mavic Mini E

<iframe src="/eager/resources/uasreg/uva_mini_e_FA3PH4RY9P.pdf" width="100%" height="250px"></iframe>


### <a name="mmS"></a>Mavic Mini S

<iframe src="/eager/resources/uasreg/sfdw_mini_FA3RHX3KHC.pdf" width="100%" height="250px"></iframe>


### <a name="mmA"></a>Mavic Mini SE A

<iframe src="/eager/resources/uasreg/uva_miniSE_a_FA3YYMCHYP.pdf" width="100%" height="250px"></iframe>


### <a name="mmB"></a>Mavic Mini SE B

<iframe src="/eager/resources/uasreg/uva_miniSE_b_FA3PH4WTCH.pdf" width="100%" height="250px"></iframe>


### <a name="mmC"></a>Mavic Mini SE C

<iframe src="/eager/resources/uasreg/uva_miniSE_c_FA3PH4YNF9.pdf" width="100%" height="250px"></iframe>


### <a name="mmD"></a>Mavic Mini SE D

<iframe src="/eager/resources/uasreg/uva_miniSE_d_FA3PH74KKX.pdf" width="100%" height="250px"></iframe>


### <a name="mmS"></a>Mavic Mini SE S

<iframe src="/eager/resources/uasreg/sfdw_miniSE_FA3WAMLXTM.pdf" width="100%" height="250px"></iframe>


### <a name="mm3A"></a>Mavic Mini 3 A

<iframe src="/eager/resources/uasreg/uva_mini3_a_FA3KY4L3KL.pdf" width="100%" height="250px"></iframe>


### <a name="hbdS"></a>Holybro Drone S

<iframe src="/eager/resources/uasreg/sfdw_holybro_FA3WAMNRXE.pdf" width="100%" height="250px"></iframe>


## Operating Manuals and Information

### DJI Mavic Pro Platinum

Product link: [LINK](https://www.dji.com/mavic-pro-platinum)

#### Software Downloads 

##### DJI GO4

[iOS](https://itunes.apple.com/us/app/dji-go-4/id1170452592?mt=8) | [Android](https://service-adhoc.dji.com/download/app/android/653957a6-85f7-4c21-8830-830300d6e3c4)

##### DJI Assistant 2

[Mac](https://dl.djicdn.com/downloads/dji_assistant/20180516/DJI+Assistant+2+1.2.4.pkg) | [Windows](https://dl.djicdn.com/downloads/dji_assistant/20190327/DJI+Assistant+2+1.2.5.exe)

#### Manuals

[User Manual](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicpro_usermanual.pdf) | [In the Box](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicpro_inthebox.pdf) | [Quick Start Guide](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicpro_quickstart.pdf) | [Battery Safety Guide](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicpro_battery.pdf)

#### Flight Limitations

| Specification         | Value                       |
|-----------------------|-----------------------------|
| Operating Temperature | 32° to 104° F (0° to 40° C) |
| Max Wind Resistance   | 10 m/s                      |

#### Battery Specifications
Product link: [LINK](https://store.dji.com/product/mavic-intelligent-flight-battery-platinum?vid=32771)
<br>Capacity: 3830 mAh
<br>Voltage: 11.4 V
<br>Battery Type: LiPo 3S
<br>Energy: 43.6 Wh
<br>Net Weight: Approx.0.5 lbs (240 g)
<br>Operating Temperature: 41° to 104° F (5° to 40° C)

### DJI Mini

Product Link: [LINK](https://www.dji.com/mavic-mini)

#### Software Downloads 

##### DJI Fly

[iOS](https://apps.apple.com/us/app/dji-fly/id1479649251) | [Android](https://service-adhoc.dji.com/download/app/android/3b03cb95-bc4b-4b62-b4ab-9cf977d77309)

##### DJI Assistant 2

[Mac](https://dl.djicdn.com/downloads/dji_assistant/20200805/DJI+Assistant+2+For+Mavic+2.0.14.pkg) | [Windows](https://dl.djicdn.com/downloads/dji_assistant/20200805/DJI+Assistant+2+For+Mavic+2.0.14.exe)

#### Manuals

#### Flight Limitations

| Specification         | Value                       |
|-----------------------|-----------------------------|
| Operating Temperature | 32° to 104° F (0° to 40° C) |
| Max Wind Resistance   | 8 m/s                       |

[User Manual](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicmini_usermanual.pdf) | [In the Box](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicmini_inthebox.pdf) | [Quick Start Guide](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/mavicmini_quickstart.pdf)

#### Battery Specifications

Product link: [LINK](https://store.dji.com/product/mavic-mini-intelligent-flight-battery?vid=84711)
<br>Model: MB2-2400mAh-7.2V
<br>Rated Capacity: 2400 mAh
<br>Battery Type: Li-ion 2S
<br>Rated Voltage: 7.2 V
<br>Limited Charge Voltage: 8.4 V
<br>Energy: 17.3 Wh
<br>Max Charging Power: 24 W
<br>Charging Temperature: 5° to 40°C (41° to 104°F)

### EcoFlow DELTA 2 Max Smart Extra Battery

Product Link: [LINK](https://us.ecoflow.com/products/delta-2-max-smart-extra-battery?variant=40539587936329)

#### Manuals

[User Manual](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/manuals/ecoflowdelta_usermanual.pdf)
