# Contingency Settings

This is a guide on how to set the contingency settings for the DJI Mini, Mini SE, and Mavic Pro Platinum

Power on the drone and get to the Go Fly view. Example:

<center>
<figure>
  <img src="../img/contingency_settings/gofly_mini_view.PNG" width="500">
</figure>
</center>

From here the steps to set the contingency settings are similar in both the DJI GO 4 app and the DJI Fly app but there are a few differences. The rest of the settings are broken up by app down below

## DJI Mini & Mini SE (Fly App)

For the Mini & Mini SE, once you are in the Go Fly view, tap the 3 dots in the upper right hand corner of the screen. This will bring you into the settings menu:

<center>
<figure>
  <img src="../img/contingency_settings/mini1.PNG" width="500">
</figure>
</center>

Once you are in the settings menu, navigate to the **"Safety"** tab. Within this section the first 3 settings will be the **"Maximum Altitude"**, **"Maximum Distance"**, and **"Auto RTH Altitude"**. Each setting should be set to the following:

- **Maximum Altitude** = 30 m AGL
- **Maximum Distance** = 100 m
- **Auto RTH Altitude** = 15 m  

<center>
<figure>
  <img src="../img/contingency_settings/mini2.PNG" width="500">
</figure>
</center>

Next, scroll to the bottom of the **"Safety"** section and click on the option called **"Advance Safety Settings"**. See below:

<center>
<figure>
  <img src="../img/contingency_settings/mini3.PNG" width="500">
</figure>
</center>

Within this setting is where you will be able to set the **"Signal Loss"** and **"Emergency Propeller Stop"** settings. Each setting should be set to the following:

- **Signal Loss** = RTH
- **Emergency Propeller Stop** = Emergency Only  
    - Emergency Propeller Stop allows you to completely shut down the aircraft by moving both control sticks towards the bottom inner or outer corners simultaneously to stop. This should only be used in extreme emergencies

<center>
<figure>
  <img src="../img/contingency_settings/mini4.PNG" width="500">
</figure>
</center>

Now you have set all contingency settings in the DJI Mini & Mini SE.

## DJI Mavic Pro Platinum (GO4 App)

For the Mavic Pro Platinum, once you are in the Go Fly view, tap the 3 dots in the upper right hand corner of the screen. This will bring you into the settings menu:

<center>
<figure>
  <img src="../img/contingency_settings/platinum1.PNG" width="500">
</figure>
</center>

Once you are in the settings menu, navigate to the **"Main Controller Settings""**. Within the Main Controller Settings section, as you scroll down the menu you will come across the **"Return-to-Home Altitude"**, **"Set Max Flight Altitude"**, **"Enable Max Distance"**, and **"Set Max Flight Distance"**. Each setting should be set to the following:

- **Return-to-Home Altitude** = 20 m
- **Maximum Altitude** = 30 m AGL
- **Enable Max Distance** = Enable
    - By default this may be disabled
- **Maximum Distance** = 100 m

See images below:

<center>
<figure>
  <img src="../img/contingency_settings/platinum2.PNG" width="500">
</figure>
</center>

<center>
<figure>
  <img src="../img/contingency_settings/platinum3.PNG" width="500">
</figure>
</center>

Just below the **"Set Max Flight Altitude"**, **"Enable Max Distance"**, and **"Set Max Flight Distance"** settings, is the **"Advance Settings"** option. Click into this setting.

Within this setting is where you will be able to set the **"Remote Controller Signal Lost"** and **"Stop Motor Method"** settings. Each setting should be set to the following:

- **"Remote Controller Signal Lost"** = Return-to-Home
- **"Stop Motor Method"** = For use in emergencies only  
    - Stop Motor Method allows you to completely shut down the aircraft by moving both control sticks towards the bottom inner or outer corners simultaneously to stop. This should only be used in extreme emergencies.  

See images below

<center>
<figure>
  <img src="../img/contingency_settings/platinum4.PNG" width="500">
</figure>
</center>

Now you have set all contingency settings in the DJI Mavic Platinum Pro.

*** 

Document written by [Dutch Byrd](mailto:byrd60@purdue.edu)