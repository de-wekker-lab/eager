# How to Download DJI Flight Records Guide (Windows/ iPhone)

After completing a flight block with either the DJI Mavic Pro Platinum, Mini, or Mini SE, a folder will be created in your iPhone called "FlightRecords". This is the folder that we need to copy over to a laptop using **ITunes**.

This guide will show you step by step how to accomplish this.

## Step #1 - Download iTunes

Navigate to [https://support.apple.com/downloads/itunes](https://support.apple.com/downloads/itunes). And click the download for **"iTunes 12.10.11 for Windows"** (32 or 64 bit) pictured below

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows.png" width="500">
</figure>
</center>

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows2.png" width="500">
</figure>
</center>

**"iTunes64(32)Setup.exe"** will now be in your computer under "Downloads". Run the setup .exe and follow the installation instructions

If the installer asks you to install or update drivers follow the steps to do so

## Step #2 - Using iTunes

Open **iTunes** and the application should look like this:

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows3.png" width="500">
</figure>
</center>

Next, plug your phone into the computer and you should notice a phone icon appear next to the music drop down menu

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows4.png" width="500">
</figure>
</center>

Click this button and you should be taken to the menu shown below

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows5.png" width="500">
</figure>
</center>

Once here, click the **"File Sharing"** Icon

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows6.png" width="500">
</figure>
</center>

From this menu you can access the data stored on your phone from different applications. If you are flying the DJI MINI or MINI SE, you will need to use the **"DJI FLY"** app shown above. If you are flying the DJI Platinum Pro, you will need to use the **"DJI GO 4"** app also shown above. 

Whether you need to use the DJI Fly or DJI GO 4 app, the rest of the process is the same. So for the rest of the tutorial we will assume a Platinum Pro is being used and will be accessing data from the DJI GO 4 app.

## Step #3 - Retrieving the data

To retrieve the flight records after a flight block or after the end of the day, click the **"DJI GO 4"** app and select the **"FlightRecords"** folder

<center>
<figure>
  <img src="../img/flightrecords_windows_ios/Itunes_Windows7.png" width="500">
</figure>
</center>

Click the **"Save"** button and select a destination on your computer to copy the FlightRecords folder to.

You have now copied the necessary flight data. Now you can clear the app cache on your phone and reset for the next flight day.

*** 

Document written by [Aaron Reilly](mailto:reilly28@purdue.edu)