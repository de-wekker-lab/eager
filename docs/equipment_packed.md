# Equipment Packed

### Aircraft

- 4 MPP 

- 4 MSE

- 1 M

### Controllers

- 8 MPP controllers

- 9 M/MSE controllers

### Batteries

- 17 MPP and 1 broken

- 13 M and 1 broken

- 6 MSE

### Props

- 5 extra MPP sets
    - 1 set for Mavic 2
- 7 extra **Individual** Mini props

### Adapters

- 13 Micro to Lightning
- 8 Micro to USB-C
- 7 Micro to Micro
- 4 USB-C - USB-A

### Charging Wires/Cases/Units

- 16 USB-A - Micro

- 10 USB-C - USB-C

- 5 USB-A - USB-C

- 3 MSE charger unit

- 2 M charger unit

- 2 MPP AC chargers

- 1 MPP XT-60 charger

- 1 MPP cig lighter charger

- 2 D6 Pro charging units

- 4 USB-C adapter blocks

- 2 M/MSE fast chargers

- 3 MPP fast chargers

- 1 4way USB-A charger

### Radios

- 3 Hand held radios

- 3 Chargers

### ToolBox

- 1 Screw driver set

- 2 needle nose pliers

- 1 Monkey wrench

- 1 Scissor

- 1 Scalpel

- 1 Label maker and 1 extra cartridge 

- 1 Multimeter

- 1 Vernier caliper

- 1 Roll electrical tape

- 1 Roll double sided tape

- 1 Box cutter

- 1 Extra pair of controller sticks

- 1 Bag of zipties

- 1 Duct tape

- 1 100ft Tape measure

- 1 Para cord

- 2 Bonus landing pad stakes

- Kris' Coins

- Solder

- 1 Hammer

### Large Field Equipment

- 2 Tents
- 14 Chairs
- 20 Vests (16 small and 4 large)
- 8 Landing pads
- 1 24 port power strip
