# DJI Mini/SE: How to Link a New Controller

1. Power on both the aircraft and the new controller

2. Connect the controller to phone or tablet device and launch the **DJI FLY** app

<center>
<figure>
  <img src="../img/mini_link_controller/img_1.png" width="500">
  <figcaption><small><small><br>The menu should be the same as picture above</small></small></figcaption>
</figure>
</center>

3. Tap the **"Connection Guide"** button in the bottom right corner

<center>
<figure>
  <img src="../img/mini_link_controller/img_2.png" width="500">
</figure>
</center>

4. Tap the **"Camera View"** button in the top right corner

<center>
<figure>
  <img src="../img/mini_link_controller/img_3.png" width="500">
</figure>
</center>

5. Tap the **"..."** menu in the top right corner

<center>
<figure>
  <img src="../img/mini_link_controller/img_4.png" width="500">
</figure>
</center>

6. Tap the **"Control"** menu and scroll all the way to the bottom

<center>
<figure>
  <img src="../img/mini_link_controller/img_5.png" width="500">
</figure>
</center>

7. Tap "Connect to Aircraft"

8. You will then be instructed to hold the power button on the aircraft for 4 seconds. After this, your controller will be paired with the aircraft.

# Changing Controllers Between Flights

1. If you see the image below while trying to pair the backup controller, tap the *"Change Bound Device"* button.

<center>
<figure>
  <img src="../img/mini_link_controller/img_8.PNG" width="500">
</figure>
</center>

2. You will be redirected to a new menu. Tap *"Next"* and  *"Confirm"* to complete the pairing process

<center>
<figure>
  <img src="../img/mini_link_controller/img_9.PNG" width="500">
</figure>
</center>

<center>
<figure>
  <img src="../img/mini_link_controller/img_10.PNG" width="500">
</figure>
</center>

!!! note

    It is very important to note that the Mavic Mini SE links the aircraft to the last person to log in to the device. This means that if you switch phones or users without logging out of the device you will not be able to pair the backup controller.


*** 

Document written by [Aaron Reilly](mailto:reilly28@purdue.edu)