# DJI GEO Zone Unlocking

## Introduction
Flights for this data campaign will take place in class E2 airspace. This area is regulated by [DJI GEO Zones](https://fly-safe.dji.com/nfz/nfz-query)

<center>
<figure>
  <img src="../img/geozone/ktph_overview.png" width="500" alt="DJI GEO Zone">
  <figcaption><small><small><br>DJI GEO Zones map showing Authorization Zone for KTPH.</small></small></figcaption>
</figure>
</center>

## Background Certification
Before you are eligible to submit or receive any GEO Unlocking licenses, you must first complete the background certification using the following steps.

1. Navigate to [DJI Flysafe website](https://fly-safe.dji.com/unlock/unlock-request/list)

<center>
<figure>
  <img src="../img/geozone/unlock_step1.png" width="500" alt="Step 1">
</figure>
</center>

2. Log in or create a DJI account.

<center>
<figure>
  <img src="../img/geozone/unlock_step2.png" width="200" alt="Step 2">
</figure>
</center>

3. If you are presented with the following text then you must proceed to complete the background certification process by first clicking the "Authentication Application" button.
```Complete required information on the Background Information page before applying to unlock```

<center>
<figure>
  <img src="../img/geozone/bgcert_step1.png" width="500" alt="Step 3">
</figure>
</center>

4. Next, select "Personal Account" and Country.

<center>
<figure>
  <img src="../img/geozone/bgcert_step2.png" width="500" alt="Step 4">
</figure>
</center>

5. Enter your personal information accurately. The phone number is used as your background certification.

<center>
<figure>
  <img src="../img/geozone/bgcert_step3.png" width="500" alt="Step 5">
</figure>
</center>

6. After you submit the application, you should receive an email from DJI stating that the verification was successful.

<center>
<figure>
  <img src="../img/geozone/bgcert_step4.png" width="500" alt="Step 6">
</figure>
</center>


## Unlock Application
!!! note
    You will not need to complete the following steps. The unlocking application will be completed by Nathan and distributed to all PICs.

1. Navigate to [DJI Flysafe website](https://fly-safe.dji.com/unlock/unlock-request/list)

<center>
<figure>
  <img src="../img/geozone/unlock_step1.png" width="500" alt="Step 1">
</figure>
</center>

2. Log in or create a DJI account.

<center>
<figure>
  <img src="../img/geozone/unlock_step2.png" width="200" alt="Step 2">
</figure>
</center>

3. Select *+ New Unlock Request*

<center>
<figure>
  <img src="../img/geozone/unlock_step3.png" width="500" alt="Step 3">
</figure>
</center>

4. Accept the "Unlocking Request Notice"

5. Select *Custom Unlocking*

<center>
<figure>
  <img src="../img/geozone/unlock_step5.png" width="250" alt="Step 5">
</figure>
</center>

6. Enter all required information in the *Basic Information* screen. This will require you to add a device by serial number and name (which will be provided to you).

<center>
<figure>
  <img src="../img/geozone/unlock_step6.png" width="500" alt="Step 6">
</figure>
</center>

7. Enter all required information in the *Custom Unlocking* screen. 
    1. Map: Click *Batch Import* and upload this file ([LINK](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/geozone/Unlock%20Template%E8%A7%A3%E7%A6%81%E6%A8%A1%E7%89%88.xlsx)).
    3. Date range: from 2023-09-10 to 2023-09-16
    4. Altitude: 100 Feet
    5. Unlocking Application Reason: Research flights located SSE of RW 33 at KTPH.
    6. Files: Upload FAA Airspace Authorization Document ([LINK](https://gitlab.com/de-wekker-lab/eager/-/raw/master/docs/resources/FAA%20Form%207711-1%202023-P107-WSA-23207_KTPH.pdf)).
    7. Submit

<center>
<figure>
  <img src="../img/geozone/unlock_step7.png" width="500" alt="Step 7">
</figure>
</center>

8. You will receive an email once the unlocking process has been completed. The unlocking license should become available through the DJI GO4/Fly app so long as you are logged into your account. Verify by selecting *GEO Zones* from the burger bar and noting "Authorization Zones: Ton..." is visible and marked *Accepted*

!!! note
       
    It may take several minutes to populate the authorization approval. Pull down from the top of the screen to refresh.

<center>
<figure>
  <img src="../img/geozone/unlock_step8.png" width="500" alt="Step 8">
</figure>
</center>