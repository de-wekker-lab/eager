# App Settings
This document explains how to clear the DJI GO4 or DJI Fly app cache before the first flight and how to adjust the settings in the app to store DAT files locally. Note that the steps below will clear **all** of the log files on your phone.

!!! note
    These steps should be completed in order. Many of the operation will become unavailable once the app is put into "Local Data Mode"

## Download Apps

### DJI Fly

[iOS](https://apps.apple.com/us/app/dji-fly/id1479649251) | [Android](https://service-adhoc.dji.com/download/app/android/3b03cb95-bc4b-4b62-b4ab-9cf977d77309)

### DJI GO4

[iOS](https://itunes.apple.com/us/app/dji-go-4/id1170452592?mt=8) | [Android](https://service-adhoc.dji.com/download/app/android/653957a6-85f7-4c21-8830-830300d6e3c4)

## Clear Cache
Before completing this step, be sure that you are logged into the DJI account that you shared in the [Attendee Info](https://purdue0-my.sharepoint.com/:x:/g/personal/rose196_purdue_edu/EbDaUUNqKshKnUhxuibfdGABcLXNviuZhRps1NNkCxK30g?e=zidmgk) spreadsheet.

### DJI Fly Steps
1. Open DJI Fly app

<center>
<figure>
  <img src="../img/clear_cache/fly_step1.jpeg" width="100" alt="Step 1">
</figure>
</center>

2. Click Profile (lower left of screen)

<center>
<figure>
  <img src="../img/clear_cache/fly_step2.png" width="500" alt="Step 2">
</figure>
</center>

3. Click Settings

<center>
<figure>
  <img src="../img/clear_cache/fly_step3.png" width="500" alt="Step 3">
</figure>
</center>

4. Click Clear Cache

<center>
<figure>
  <img src="../img/clear_cache/fly_step4.png" width="500" alt="Step 4">
</figure>
</center>

5. Clear App Cache, Aircraft Flight Record, Local Log

<center>
<figure>
  <img src="../img/clear_cache/fly_step5.png" width="500" alt="Step 5">
</figure>
</center>

### DJI GO4 Steps
1. Open DJI GO4 app

<center>
<figure>
  <img src="../img/clear_cache/go4_step1.jpeg" width="100" alt="Step 1">
</figure>
</center>

2. Click burger bar (top right of screen)

<center>
<figure>
  <img src="../img/clear_cache/go4_step2.png" width="250" alt="Step 2">
</figure>
</center>

3. Click Flight Records

<center>
<figure>
  <img src="../img/clear_cache/go4_step3.PNG" width="250" alt="Step 3">
</figure>
</center>

4. Click cloud sync icon (top right of screen)

<center>
<figure>
  <img src="../img/clear_cache/go4_step4.png" width="250" alt="Step 4">
</figure>
</center>

5. Clear local flight records

<center>
<figure>
  <img src="../img/clear_cache/go4_step5.png" width="250" alt="Step 5">
</figure>
</center>

6. Clear all flight records

<center>
<figure>
  <img src="../img/clear_cache/go4_step6.png" width="250" alt="Step 5">
</figure>
</center>

## Applying Geo Unlocking
Before completing the next section, [Set Local Data Mode](#set-local-data-mode), you must apply for and receive geo unlocking for the KTPH area. The operational manager will apply for this using your DJI account email shared in the [Attendee Info](https://purdue0-my.sharepoint.com/:x:/g/personal/rose196_purdue_edu/EbDaUUNqKshKnUhxuibfdGABcLXNviuZhRps1NNkCxK30g?e=zidmgk) spreadsheet. Once this step has been completed, you will upload the GEO Unlock to the aircraft following the steps below.

### DJI Fly Steps
<center>
<video width="640" height="296" controls>
  <source src="../img/app_settings/geo_fly.MP4" type="video/mp4">
  Your browser does not support the video tag.
</video>
</center>

1. From the home screen, click "Profile"
2. Scroll down and select "Settings"
3. Scroll down and select "Unlock GEO Zone"
4. Click the refresh icon in the upper right corner of the screen to load in new unlock licenses.
5. Apply the appropriate license.

### DJI GO4 Steps
<center>
<video width="296" height="640" controls>
  <source src="../img/app_settings/geo_go4.MP4" type="video/mp4">
  Your browser does not support the video tag.
</video>
</center>

1. From the home screen, click "Me"
2. Select "More"
3. Select "My Unlock NFZ Application"
4. Click "Refresh" to load in new unlocking licenses and apply accordingly


## Set Local Data Mode
Only complete this section once you have completed the GEO Zone Unlocking. This section covers how to adjust the DJI app settings to prevent the DAT files from uploading to the DJI cloud and being removed from your device. This step is **critical** to the data collection process. 

### DJI Fly Steps
<center>
<video width="640" height="296" controls>
  <source src="../img/app_settings/local_fly.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
</center>

1. From the home screen, click "Profile"
2. Scroll down and select "Settings"
3. Select "Sync Flight Data" and turn **off** "Auto-sync Flight Records"
4. Next, select "Privacy" and turn **on** "Local Data Mode"
5. Confirm the setting change and restart the app.

### DJI GO4 Steps
<center>
<video width="296" height="640" controls>
  <source src="../img/app_settings/local_go4.MP4" type="video/mp4">
  Your browser does not support the video tag.
</video>
</center>

1. From the home screen, click "Me" in the lower right.
2. Select the gear icon in the upper right to open settings.
3. Next, select "Privacy" and turn **on** "Local Data Mode"
4. Confirm the setting change and restart the app.

