# Airspace Training

## Airport Synopsis:

- Tonopah Airport (KTPH) is an untowered airport located within surface Class E airspace (see [Figure 1](#figure-1-sectional-chart-view) in appendix)
    - IFR traffic are required to operate with ATC clearance
    - VFR traffic are not under ATC control
- Consists of 2 runways in a V-configuration (see [Figure 2](#figure-2-ktph-airport-diagram)):
    - Runway 11-29: 5,660’ x 50’
        - Smaller with poor quality asphalt; less likely to be used
    - Runway 15-33: 7,160’ x 75’
        - Longer and wider with excellent asphalt; most likely to be used
- Averages 134 operations per week (based off annual statistics ending in May 2022)
    - 53% are transient GA aircraft, 26% air taxi, 15% local aircraft, 6% military
        - We can expect the majority of aircraft to be operating at speeds of ~100 kts or less
    - Implies a daily average of 20 operations
- Most aircraft use this as a fuel stop, however there are 7 aircraft based here:
    - 5 single-engine
    - 1 multi-engine
    - 1 helicopter
- Parachute activity over the town of Tonopah 

## Untowered Field Communications:

- Arriving and departing VFR aircraft are not required to communicate
    - It is the pilot’s responsibility to “see and avoid”
    - That being said, most pilots follow the best practice of monitoring and making radio calls on CTAF (Common Traffic Advisory Frequency)
- Multiple airports may be covered by the same CTAF frequency, so each radio call is normally initiated with the phrase “[name of airport] traffic”
    - Relevant radio calls at KTPH will therefore begin with “Tonopah traffic”
- Radio calls will generally follow the same basic formula:
    1. Who they’re calling
        1. AKA “Tonopah traffic”
    2. Who they are
        1. Either a tail number or model of aircraft
    3. Where they are
        1. Position of the aircraft relative to the airfield (10 miles north, downwind for runway X, etc...)
    4. What they want (i.e. their intentions)
    5. Any other pertinent information
- Arriving and departing IFR traffic are required to be in contact with ATC 
    - They will typically switch to CTAF frequency when ATC permits (usually within 10 miles of the airport)

## Airport Traffic Patterns:

- Unless otherwise authorized by the FAA, all aircraft are required to follow a standard left-hand traffic pattern (see [Figure 3](#figure-3-standard-left-traffic-pattern))
    - This applies to KTPH
- Our operations area places us to the south of the airport, just before the runway 33 threshold (see [Figure 4](#figure-4-uas-operations-area-polygon))
    - As such, our primary concern is landing traffic on runway 33 and departing traffic on runway 15
- Traffic pattern altitude is 1000’ AGL for GA aircraft, and 1500’ AGL for heavier/turbine aircraft
    - Lateral distance from the airport varies based on the same parameters, but is typically between 1-2 NM
- Depending on the aircraft’s position, they may forgo the traffic pattern and fly straight-in to the runway on a long final approach leg
- An example of a left-hand traffic pattern at KTPH can be seen in [Figure 5](#figure-5-ktph-left-traffic-pattern)

## Instrument Approaches

- Two approaches exist at KTPH:
    - RNAV Rwy 15
        - Directs IFR traffic from the north to a straight-in for runway 15
    - VOR or GPS-A
        - Uses the TPH VOR navaid southeast of the field to direct traffic to the airport for a “circle-to-land" approach
            - “circle-to-land" approaches allow the pilot to choose which runway they wish to land on
        - The circling altitude can be as low as 670’ AGL, well below pattern altitude
            - Because of the low altitude maneuvering, this type of approach is the most risky

## How to Look for Traffic:

- Focus on scanning small, 10° segments of the sky for at least 1 second 
    - After scanning an area of sky, refocus your eyes by looking at an object at a closer distance before resuming your scan
- Use sunglasses or a hat to prevent the sun from blinding you
- Use binoculars to aid in identifying traffic, but maintain situational awareness
- Monitor CTAF, [Flightradar24](https://www.flightradar24.com/38.01,-117.08/12), and other tools for help

## Figures

### Figure 1: Sectional Chart View

<center>
<figure>
  <img src="../img/training_airspace/figure1.png" width="500">
</figure>
</center>

### Figure 2: KTPH Airport Diagram

<center>
<figure>
  <img src="../img/training_airspace/figure2.png" width="500">
</figure>
</center>

### Figure 3: Standard Left Traffic Pattern

<center>
<figure>
  <img src="../img/training_airspace/figure3.png" width="500">
</figure>
</center>

### Figure 4: UAS Operations Area Polygon

<center>
<figure>
  <img src="../img/training_airspace/figure4.png" width="500">
</figure>
</center>

### Figure 5: KTPH Left Traffic Pattern

<center>
<figure>
  <img src="../img/training_airspace/figure5.png" width="500">
</figure>
</center>


*** 

Document written by [Spenser Havranek](mailto:shavrane@purdue.edu)