# Field Notes

[TOC]

### Day 1 - 11 September 2023

Drone Layout for Day 1-3:

| Sonic Anemometer Tower | Sonic Anemometer Tower | Sonic Anemometer Tower | Sonic Anemometer Tower | Sonic Anemometer Tower | Sonic Anemometer Tower | Sonic Anemometer Tower | Sonic Anemometer Tower |
| ---------------------- | ---------------------- | ---------------------- | ---------------------- | ---------------------- | ---------------------- | ---------------------- | ---------------------- |
| Mini SE A              | Platinum A             | Mini SE B              | Platinum B             | Mini SE C              | Platinum C             | Mini SE D              | Platinum D             |

- Approximate distance from Tower to Tower: 5 Meters

- Approximate distance from Midpoint (between two towers) to the Center of Landing Pad: 4 Meters

- Compass calibration was performed with 1 battery. Calibration was done at a safe distance from all nearby metal objects. No other calibrations were performed. 

- Mini SE A will be using gray batteries. All other minis will be using black batteries.

- Mavic Platinum B has a lousy gimbal and a broken back left arm. 

- We will be locking phones while in flight to help conserve battery.  

  &nbsp;    

- 1st Flight: 11:10 - 11:25 

- 2nd Flight: 11:48  - 12:03 

- 3rd Flight :  12:16 - 12:31 
  
  - Mini SE B experienced connection issues during 3rd flight.

- 4th Flight: 14:39 - 14:55 

- 5th Flight: 15:03 - 15:22 

- 6th Flight: 15:29 - 15:48 
  
  - Platinum C had a delayed takeoff on 6th takeoff caused by a battery issue with MPP13

- 7th Flight: 15:55 - 16:15 

- 8th Flight: 16:54 - 17:12
  
  - During 8th flight, Mini SE B flew with Local Data Mode off and Autosync off
  
  - Battery MPP13 had issues again. It will no longer be used.

- 9th flight: 17:40 - 17:58
  
  - We are no longer operating phones in low power mode
  
  - Platinum B flew with Local Data off and Cellular Data to Upload off 
  
  - All Minis flew with Local Data off and Autosync off
  
  - During 9th flight, the iphone controlling Mini SE A spontaneously shutoff mid flight. Drone did not stop flying.

### Day 2 - 12 September 2023

- Minis will be flying with Local Data off and Autosync off

- Platinums will be flying with Local Data off and Cellular Data to Upload off

- All drones will now be flying with the following conditions
  
  - Airplane mode on for PICs 
  
  - Leave phone on/unlocked
  
  - No power saving modes enabled, i.e. Low Power Mode
  
  - No background apps running
  
  - Good connection between phone and controller
  
  - Phone brightness turned down to conserve battery  
  
  &nbsp;   

- 1st Flight: 12:28- 12:45 

- 2nd Flight: 12:50 - 13:08

- 3rd Flight: 13:12 - 13:31

- 4th Flight: 13:34 - 13:53 

- 5th Flight: 13:56 - 14:06
  
  - Plane landed on Runway 33 during 5th flight causing an early grounding of all drones

- 6th Flight: 15:53 - 16:11

- 7th Flight: 16:15 - 16:24
  
  - Plane tookoff during 7th flight causing an early grounding of all drones

- 8th Flight: 16:30 - 16:49 
  
  - During 8th flight, Mini SE B disconnected at some point

- 9th Flight: 16:54 - 17:13

- 10th Flight: 17:41 - 18:00

- 11th Flight: 18:10 - 18:30

### Day 3 - 13 September 2023

- For the flights today we will be alternating between:
  
  - All drones orientated south
  
  - All drones orientated in 60° increments
    
    - Platinum and Mini A's will be facing 180°
    
    - Platinum and Mini B's will be facing 240°
    
    - Platinum and Mini C's will be facing 300°
    
    - Platinum and Mini D's will be facing 360°

- All flights should be assumed to be orientated south unless otherwise specified

- Cass is now the VO with Luke

- Spencer is now the PIC with Austen
  
  &nbsp;   

- 1st Flight: 11:18 - 11:36

- 2nd Flight: 11:47 - 12:04 
  
  - This flight was flown with angled increments 

- 3rd Flight: 12:08 - 12:26

- 4th Flight: 12:41 - 12:59 
  
  - This flight was flown with angled increments

- 5th Flight: 13:15 - 13:33

- 6th Flight: 13:37 - 13:56
  
  - This flight was flown with angled increments

- 7th Flight: 15:52 - 15:57
  
  - Flight 7 landed early due to heavy wind/rain

- 8th Flight: 16:26 - 16:42 
  
  - For flight 8 and onwards, all Platinums will be using backup controller as primary

- 9th Flight: 17:31 - 17:50 

- 10th Flight: 17:57 - 18:13 
  
  - This flight was flown with angled increments

- 11th Flight: 18:16 - 18:36
  
  - This flight was flown with angled increments 
  
  - During 11th flight, Mini B experienced connection issues causing a return-to-home

### Day 4 - 14 September 2023

Drone Layout for Day 4-5:

|           | Sonic Anemometer Tower |            |
| --------- | ---------------------- | ---------- |
| Mini SE A |                        | Platinum A |
| Mini SE B |                        | Platinum B |
| Mini SE C |                        | Platinum C |
| Mini SE D |                        | Platinum D |

- All drones cleared cache prior to 1st flight, no other adjustments made.
  
  &nbsp;   

- 1st Flight: 11:30 - 11:48 
  
  - Minis and Mavics seemed offset in height. Minis seemed to be higher 

- 2nd Flight: 11:59 - 12:17
  
  - Mini B was temporarily unable to takeoff on 2nd flight, error code 30064. Solution was to turn off airplane mode with binding controller connected to aircraft. 

- 3rd Flight: 12:26 - 12:45

- 4th Flight: 12:54 - 13:06

- 5th Flight: 13:13 - 13:29
  
  - Flight 5 was a test flight. All drones flew at 10 meters and the platinums corrected based on the Minis. The results showed that Platinum B and D were offset by positive 3 meters. Platinum A was offset by positive 2 meters. Platinum C was offset by positive 1 meter. 

- 6th Flight: 13:33 - 13:51 
  
  - For flight 6, all platinums flew with a positive 2 meter offset. 

- 7th Flight: 13:58 - 14:17
  
  - For flight 7, after 8 minutes, all platinums manually realigned their altitude with Minis and recorded offset. 

- 8th Flight: 17:12 - 17:30
  
  - For flight 8 and all flights afterwards, platinums continuously corrected their altitude to match the altitude of their partner mini. Corrections were made when needed and only when the platinum(s) exceeded plus or minus 3 feet in altitude from the mini. Adjustments were slow and constant. 

- 9th Flight: 17:37 - 17:56

- 10th Flight: 18:03 - 18:21

- 11th Flight: 18:27 - 18:47

### Day 5 - 15 September 2023

- For all flights today:
  
  - Group A and C will ascended at a constant rate of 1 m/s from 2 m to 28 m. Once they reach the top they will descend as quickly as possible. They will reconvene at the top and the bottom.
  - Group B and D will ascend to 2m, assure everyone is synced, then ascend to 4m, hover for a period, then ascend to 7m, hover for a period, then ascend to 15m, hover for a period, then ascend to 28m and hover for a period. Once they are done hovering they will descend to 2m as quickly as possible and repeat. 
    - They will determine their altitude by using the equation: Indicated height - true height + assigned height = final height. 

- Mini SE D recalibrated it's IMU prior to 1st flight.
  
  &nbsp;   

- 1st Flight: 11:02 - 11:18

- 2nd Flight: 11:22 - 11:41
  
  - For the 2nd - 6th flight, group B and D will be hovering for 30 seconds instead of 60 seconds.
  
  - After the 2nd flight, Mini A's iphone randomly powered off post flight.

- 3rd Flight: 11:53 - 12:10 
  
  - For 3rd flight, Platinums B and D will add 2,3,9,14 to their height instead of 2,3,8,13. Minis will continue to add 2,3,8,13.

- 4th Flight: 12:16 - 12:32
  
  - For 4th flight onwards, Platinums  B and D will manually align their altitude with their partner mini. 

- 5th Flight: 12:40 - 12:57 
  
  - Coordination issues prior to flight.

- 6th Flight: 13:11 - 13:29 

- 7th Flight: 15:35 -15:54 
  
  - For 7th flight and onwards, All B's and D's will hover for 10 seconds instead of 30 seconds. All A's and C's will ascend at 0.5-1 m/s (ideally 0.5 m/s). 
  
  - For 7th flight, mini C descended very late on one of the descents. 

- 8th Flight: 15:58 - 16:15 
  
  - For 8th flight onwards, A's and C's will cycle through ascending at 0.5,1,2 m/s. 

- 9th Flight: 16:21 - 16:40 

- 10th Flight: 16:45 - 17:03

- 11th Flight: 18:00 - N/A 
  
  - For 11th flight, drones flew several transects along the towers up to tower #25. Platinums flew in front of the Minis. Once the 25th tower was reached, Platinums continued onwards to the 28th tower while Minis stopped at the 25th tower and ascended 3 meters. Both drones then flew back as quickly as possible and repeated the transects. 

***

Notes is this section are written and recorded by [Westin Miley](mailto:wpmiley@purdue.edu).