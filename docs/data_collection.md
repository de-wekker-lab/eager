# Data Collection

## Introduction

Data collection will be managed by Nathan, Aaron, and Dutch. Data sets will be downloaded after each flight, stored locally on a PC and uploaded to a cloud folder. [LINK](https://purdue0-my.sharepoint.com/:f:/g/personal/rose196_purdue_edu/Eot-Z0QpmG1BmTK9iSnxU2MBkG-56tba6fo_nyDRbVafvw?e=lXhgqN)

## Collection Process
Follow appropriate instruction set under the "Extract Flight Records" tab in the main menu.

Once the **"FlightRecords"** folders are extracted, rename them to fit the following format ***"FlightRecords_droneidentifier_day.flight"***. The day and flight portions will be named numerically. Heres an example of a folder:

**FlightRecords_MPP6_1.2**

    Name breakdown:
    - FlightRecords = Folder name
    - MPPA = From the Mavic Platinum Pro A
    - 1.2 = Refers to day 1 & 2nd flight block of the day  

## Flight Log

Visual Observers will record the following information for each flight by paper and then complete the [Flight Log](https://purdue0-my.sharepoint.com/:x:/g/personal/rose196_purdue_edu/ETrX6SV0bMZJglDvitd1KOwB89-BTYLAgjGTkB19_AnVWA?e=F5T0ng) spreadsheet every evening. :

    - Date
    - Flight Number
    - Battery Number
    - Important flight notes