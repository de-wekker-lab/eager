# Desert Training

## Attire
- Thin and light, long-sleeves/pants
- Closed toe shoes/hiking boots
- Sunglasses
- Hat (“boonie” type preferred)
- Sunscreen
- Light jacket or sweatshirt for the mornings/evenings

## Environmental Dangers/Considerations
- Sun exposure
    - While it may not seem apparent at first, sunburns will only worsen throughout the week if you're skin is exposed. Be proactive – wear sunscreen or cover up. 
- Heat exhaustion
    - Make sure you're drinking plenty of water and staying hydrated. Temps are projected to be moderate-warm for the week we are there, but we’ll be outdoors the entire day.
- Insects
    - Bees/wasps, mosquitos, scorpions – these are straightforward. Keep your distance if you see them and avoid messing with them. 
- Snakes
    - There are a handful of venomous snakes in NV. Keep your distance if you spot one. If you hear one rattling, stop moving until you can identify where the snake is, then carefully remove yourself from the area.

## In Case of Emergency
- Local medical facilities
    - Nearest Urgent Care – [Frontier Medical Group](https://www.fmgclinic.com/)
        - [775-382-2000](tel:+17753822000)
        - [825 S Main St, Tonopah, NV 89049](https://goo.gl/maps/WKPwJpuDfik5BcMf9)
    - Nearest Hospital is **100 miles away** – for life threatening emergencies, call 911
- Snake Bite Care
    - If you are bitten by a venomous snake, remain calm. Physical exertion will increase your heart rate, leading to faster distribution of venom. Safely take a picture of the snake if you can for identification purposes and head to the nearest medical facility.
    - Attempting to kill the snake is probably not the best idea. Even the severed head of a snake can still bite with venom.
- Scorpion Sting Care
    - Most scorpion stings are not lethal to healthy adults. Depending on the scorpion, symptoms can range anywhere from minor pain, tingling, swelling and redness, to severe pain accompanied by vomiting and shortness of breath.
    - Apply ice to the sting site to help with pain and swelling.
    - If you seem to be having an allergic reaction, seek medical attention immediately. 

## Good Habits
- Make sure you have what you need for the day BEFORE you get out into the field
    - Bring extra sunscreen, water, snacks, etc.

## Bad Habits
- Playing with wildlife
- Forgetting your essentials back at the hotel
- Not wearing sunscreen

*** 

Document written by [Karl Oversteyns](mailto:koverste@purdue.edu) and [José Ramírez](mailto:ramireja@purdue.edu)