# Travel Information

Please review this information below for tips about travel and the itinerary.

## Google Maps Shortcuts
### [FastPark Indy](https://goo.gl/maps/ZUGcBScKnkLbxdh99)
### [AirBnB](https://goo.gl/maps/MoQNpv89afoToE2VA)
### [Belvada Hotel](https://goo.gl/maps/qxgCwKfVknjTB2Um8)

## Frontier Flight Check In
You will need to check into your flight within 24 hours of departure. To do this you will need the Frontier App and your confirmation number. Follow these steps:

1. In the app, click the "My Trip" tab at the bottom of the screen.

<center>
<figure>
  <img src="../img/travel/frontier_app_1.jpeg" width="250" alt="Step 1">
</figure>
</center>

2. Input your confirmation number and last name. Your confirmation is available in the 'Attendee Information/Assignments' tab. This should pull up your flight and load it into you app.

<center>
<figure>
  <img src="../img/travel/frontier_app_2.jpeg" width="250" alt="Step 1">
</figure>
</center>

3. Return to this screen within 24 hours before your flight to check in. This app will also include your boarding ticket. 

## Driving to FastPark Indy
In order to get to the airport in time for the flight which departs at 1:49 pm ET, you should plan on leaving Purdue by 10:30 am. Plan to coordinate with your driver in advance to work out the pick up details. 

When you arrive at FastPark Indy you will scan in with the barcode provided to your driver. This is your reservation. The attendant will give you directions on parking and a shuttle will meet you at your vehicle. The shuttle driver will give you a ticket with your parking spot number. Save this card and take a photo of it so that you can be returned to your car at the end of the trip.

FastPark is located at:
[8550 Stansted Rd
Indianapolis, IN 46241](https://www.google.com/maps?q=8550%20Stansted%20Rd,Indianapolis,46241)

## Arrival at KIND
The shuttle will drop you at the lower level of the Indy airport (rental car area) head up the stairs and head to either concourse A or B to go through security. Once through security, you will follow the screens and the signs to your gate. Travel as a group and you will have no problem getting to the right place! You will be called to board your flight by your boarding number available on your boarding pass from the Frontier app. Enjoy the flight!

## KIND to KLAS
DEPARTING FLIGHT 985 
Indianapolis (IND) to Las Vegas (LAS) 	
Depart: 9/10/2023 1:49 PM | Arrive: 9/10/2023 2:54 PM
Total Duration: 4 hr 5 min 

When you arrive in Las Vegas at , follow signs to the vehicle rental area. You will need to take transportation to get to the rental vehicle area. See the video below for information. Aaron and Nathan will meet you at this location with the Purdue UAS van and a rental minivan. 
<center><iframe width="560" height="315" src="https://www.youtube.com/embed/vM2piExd-sg?si=2m0A3noKBDWp_4_f" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></center>


## Arrival at KTPH
From KLAS to KTPH is a 3.5-4 hour drive.

## KLAS to KIND
RETURNING FLIGHT 2038
Las Vegas (LAS) to Indianapolis (IND)	
Depart: 9/16/2023 2:39 PM | Arrive: 9/16/2023 9:30 PM
Total Duration: 3 hr 51 min

We will drive from KTPH to KLAS in time to board the flight back to KIND. You will be dropped at the departures gate.

## Housing
Room assignment will be made in Tonopah. below is an overview of what is available:
- [AirBnB](https://goo.gl/maps/MoQNpv89afoToE2VA)
    - Sleeps 6
- [Belvada Hotel](https://goo.gl/maps/qxgCwKfVknjTB2Um8)
    - 4 Double Queen Rooms
    - 2 Single Queen Rooms

## Food
All food will be covered. Breakfast will be on the go (granola bars, fruit, etc.), lunch will be at the field site (sandwiches, chips, veggies, etc.), and dinner will be out at a local restaurant at the end of the day. 

