# Risk Management

## Introduction

Because there are certain inherent risks associated with travel and field research work, students must be familiar with the rules and regulations of operating safely in the field. This information has been presented in the trip training which is available under the "Training" subheader in the main menu for this website.

This page also includes official Purdue University Risk Management documentation required for this trip. Please read and/or sign required documentation.

## Waiver, Release and Hold Harmless Agreement

All participants must complete the [RM29](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=86627267-d226-406e-a1e6-7b6fa31befbd&env=na2&acct=9ad6adfd-6804-409b-91bc-173cbee909f9&v=2) form prior to travel. 

## Camp, Conference, and Field Trip Insurance

All participants are covered by an official grant funded primary insurance administered by Purdue University. This insurance should be used in the event of any medical emergencies. Although students are not expected to be familiar with this policy, the information is linked [here](https://www.purdue.edu/business/risk_mgmt/pdf/Camp,%20Conference,%20and%20Field%20Trip%20Insurance.pdf#Camp,%20Conference,%20and%20Field%20Trip) should students wish to review the insurance available to them for the duration of this trip. 

