# Pre-trip Preparations

## Introduction
Campaign participants must complete the items below as assigned.

## Training
Participants will be expected to participate in an in-person two-hour training session to review the project overview and the operational limitations/procedures.

Training materials are available under the Training subheading in the main menu of this website.

## Risk Management
Participants are required to complete the forms under the [Risk Management](risk_management.md) page.

## Student Excused Research Absence
Students are expected to work with their professors regarding their absence for the week of 10-16 September. Students are encouraged to preemptively present professors with a plan for how they will complete the work. If you wish, please use this [Research Absence Letter](https://purdue0-my.sharepoint.com/:b:/g/personal/rose196_purdue_edu/EVVgfZmcWkdBh4q27DckAA8BuIEcZ8jVBfToH7_7JpGiZA?e=hhHDPM) to share with your professors to help verify the request. 

## [UAS Prep Checklist](https://to-do.office.com/tasks/l-1lbwjf6qqiq)
To edit this list, follow this [invitation link](https://to-do.microsoft.com/tasks/sharing?InvitationToken=pNIrBxcuGUZWrMdrGGxBSg_VaIpClASFthoILzxyTTMjyY8Zo3eNh3Ivad29_V0Xk)

## Student To Do List

??? note "Todo:"
    - [ ] Complete [Purdue RM29 form](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=86627267-d226-406e-a1e6-7b6fa31befbd&env=na2&acct=9ad6adfd-6804-409b-91bc-173cbee909f9&v=2)
    - [ ] Download and install phone apps. See the [Aircraft Information](aircraft.md) page.
    - [ ] Download [iTunes](https://www.apple.com/itunes/) (PC only)
    - [ ] Adjust App Settings. See [App Settings](app_settings.md) page.
    - [ ] Review aircraft operating manuals. See the [Aircraft Information](aircraft.md) page.
    - [ ] Review [Operation Guide](operation.md).
    - [ ] Perform DJI GEO Zone unlocking. See the [GEO Zone Unlocking](geozone.md) page

## [Equipment Packing List](https://to-do.office.com/tasks/l-1yfcri1diky)
To edit this list, follow this [invitation link](https://to-do.microsoft.com/tasks/sharing?InvitationToken=DeL54bxJ6gzsbkuFQMMkQAr957D3PDi0oN7Q1GGP1pSu6JNMkWNDYami1eAKj2ZO4)

## [Equipment Packed](equipment_packed.md)

## [Packed Items List](https://to-do.office.com/tasks/l-262wvxqvqnp)
This list is only accessible by Nathan and Aaron.

## [Student Packing List](https://to-do.office.com/tasks/l-1sl35f4drny)
To edit this list, follow this [invitation link](https://to-do.microsoft.com/tasks/sharing?InvitationToken=wmQaOG02roV8lpgS1MEEEQgBDSxaetAOkXB1GXucn8g69xbUqQCgsivChO_vNeRcM)
