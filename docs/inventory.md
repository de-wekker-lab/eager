# Inventory

**Aircraft:**

- 4 DJI MAVICPRO Platinum (MPP) - A,B,C,D

- 4 DJI MINI SE (MSE) - A,B,C,D

- 1 DJI MINI (M) - E

**Controllers:**

- 8 MPP controllers - A,B,C,D,A1,A2,A3,A4

- 8 MSE controllers - A,B,C,D,A1,A2,A3,A4

- 1 M controller -E

**Batteries:**

- 8 MPP batteries + 9 MPP batteries from UVA

- 6 MSE batteries

- 9 M batteries

**Chargers:**

- 4 MPP chargers + 9 MPP chargers from UVA

- 3 MSE chargers + USB-A to USB-C + 8 MSE chargers from UVA

- 1 M charger (holds 3 but charges 1 at a time) + 3 USB-A to Micro

**Propelers:**

- 5 Extra full sets - MPP

- 3 Extra full sets - MSE

- 3 Extra full sets - MS

**Adapters:**

- 12 Micro to Lightning - *5 Lightning cables belong to Purdue*

- 7 Micro to USB-C

- 6 Micro to Micro
