# How to Download DJI Flight Records Guide (Mac/ iPhone)

After completing a flight block with either the DJI Mavic Pro Platinum, Mini, or Mini SE, a folder will be created in your iPhone called "FlightRecords". This is the folder that we need to copy over to a laptop.

This guide will show you step by step how to accomplish this.

## Step #1 - Navigating to Application Menu

Plug your phone into the computer and open finder 

In the locations section, click on the device you want to extract the files from. In this example, we are using **"Dutch's iPhone"**: 

<center>
<figure>
  <img src="../img/flightrecords_mac_ios/finder_mac.png" width="500">
</figure>
</center>

Next, click the **Files** section tab in the devices menu on the laptop:

<center>
<figure>
  <img src="../img/flightrecords_mac_ios/finder_mac1.png" width="500">
</figure>
</center>

From this menu you can access the data stored on your phone from different applications. If you are flying the DJI MINI or MINI SE, you will need to use the **"DJI FLY"** app shown above. If you are flying the DJI Platinum Pro, you will need to use the **"DJI GO 4"** app also shown above. 

<center>
<figure>
  <img src="../img/flightrecords_mac_ios/finder_mac2.png" width="500">
</figure>
</center>

Whether you need to use the DJI Fly or DJI GO 4 app, the rest of the process is the same. So for the rest of the tutorial we will assume a Platinum Pro is being used and will be accessing data from the DJI GO 4 app.

## Step #2 - Retrieving the data

To retrieve the flight records after a flight block or after the end of the day, click the **"DJI GO 4"** app and select the **"FlightRecords"** folder

<center>
<figure>
  <img src="../img/flightrecords_mac_ios/finder_mac3.png" width="500">
</figure>
</center>

Now drag the **"FlightRecords"** folder to your desired destination on your computer to copy the FlightRecords to

You have now copied the necessary flight data. Now you can clear the app cache on your phone and reset for the next flight day.

*** 

Document written by [Dutch Byrd](mailto:byrd60@purdue.edu)